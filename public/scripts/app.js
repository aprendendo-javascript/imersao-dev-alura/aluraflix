//Constants
const BLINK_RATE = 400;
const BLINK_N_TIMES = 3;
const movies = [
  {
    name: "Rogue One: Uma História Star Wars",
    poster: "https://m.media-amazon.com/images/M/MV5BMjEwMzMxODIzOV5BMl5BanBnXkFtZTgwNzg3OTAzMDI@._V1_UX182_CR0,0,182,268_AL_.jpg",
    imdbLink: "https://www.imdb.com/title/tt3748528/?ref_=fn_al_tt_1"
  }, {
    name: "O Máskara",
    poster: "https://m.media-amazon.com/images/M/MV5BOWExYjI5MzktNTRhNi00Nzg2LThkZmQtYWVkYjRlYWI2MDQ4XkEyXkFqcGdeQXVyNTAyODkwOQ@@._V1_UX182_CR0,0,182,268_AL_.jpg",
    imdbLink: "https://www.imdb.com/title/tt0110475/?ref_=fn_al_tt_1"
  }, {
    name: "Uma Mente Brilhante",
    poster: "https://m.media-amazon.com/images/M/MV5BMzcwYWFkYzktZjAzNC00OGY1LWI4YTgtNzc5MzVjMDVmNjY0XkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_UX182_CR0,0,182,268_AL_.jpg",
    imdbLink: "https://www.imdb.com/title/tt0268978/?ref_=fn_al_tt_1"
  }, {
    name: "Dia de Treinamento",
    poster: "https://m.media-amazon.com/images/M/MV5BMDZkMTUxYWEtMDY5NS00ZTA5LTg3MTItNTlkZWE1YWRjYjMwL2ltYWdlL2ltYWdlXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_UX182_CR0,0,182,268_AL_.jpg",
    imdbLink: "https://www.imdb.com/title/tt0139654/?ref_=fn_al_tt_1"
  }
];
const movieSelectHtml = document.getElementById("movie-select");
const containerHtml = document.getElementsByClassName("container").item(0);

//Functions
const addMovieToSelect = (movie, index) => {
  const newElement = document.createElement("option");

  newElement.value = index;
  newElement.innerText = movie.name;
  movieSelectHtml.appendChild(newElement);
}
const populateMoviesToSelect = () => {
  movies.forEach(addMovieToSelect);
}
const appendPoster = () => {
  if (movieSelectHtml.value != "") {
    const posterBox = document.createElement("div");
    const posterToAdd = document.createElement("img");
    const movieNameH = document.createElement("h4");
    const imdbMovieLink = document.createElement("a");

    posterBox.className = "poster-box";
    posterBox.id = movies[movieSelectHtml.value].name;
    posterToAdd.src = movies[movieSelectHtml.value].poster;
    posterToAdd.className = "poster";
    movieNameH.innerText = movies[movieSelectHtml.value].name;
    movieNameH.className = "poster-title";
    imdbMovieLink.href = movies[movieSelectHtml.value].imdbLink;
    imdbMovieLink.target = "_blank";
    imdbMovieLink.className = "link";

    imdbMovieLink.appendChild(posterBox);
    posterBox.appendChild(posterToAdd);
    posterBox.appendChild(movieNameH);
    containerHtml.appendChild(imdbMovieLink);
  }
};
const getPoster = (movie) => {
  if (movie !== undefined) {
    const addedPosterBoxes = containerHtml.getElementsByTagName("div");
    for (let i = 0; i < addedPosterBoxes.length; i++) {
      if (addedPosterBoxes[i].id == movie.name) {
        return addedPosterBoxes[i];
      }
    }
  }
  return null;
};
const blinkPosterBorder = (posterBox) => {
  let timesExec = 0;
  const timerInterval = setInterval(() => {
    if (posterBox.className == "poster-box-sel") {
      posterBox.className = "poster-box";
    } else {
      posterBox.className = "poster-box-sel";
    }
    if (timesExec++ == BLINK_N_TIMES + 2) {
      clearInterval(timerInterval);
    }
  }, BLINK_RATE);
};
const appendPosterIfAbsent = () => {
  const presentPoster = getPoster(movies[movieSelectHtml.value]);
  if (presentPoster == null) {
    appendPoster();
  } else {
    blinkPosterBorder(presentPoster);
  }
};
const removePoster = () => {
  const moviePoster = getPoster(movies[movieSelectHtml.value]);
  if (moviePoster != null) {
    containerHtml.removeChild(moviePoster.parentNode);
  }
}

//Code
populateMoviesToSelect();